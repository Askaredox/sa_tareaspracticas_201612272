from flask_restful import Resource, Api
from flask import Flask, request
from flask_cors import CORS
import simplejson as json
from database import Sql
from pymysql.err import IntegrityError



app = Flask(__name__)
cors = CORS(app, resources={r"/*": {"origin": "*"}})
api = Api(app)
db = Sql()


@api.resource('/')
class Check(Resource):
    def get(self):
        return {'ok': True}

@api.resource('/estudiante', '/estudiante/<int:id>')
class Estudiante(Resource):
    def get(self, id):
        result = db.get_estudiante(id)
        return {'status': 0, 'data':result}, 200
    def post(self):
        content = request.json
        necessary_data = [
            'carnet',
            'nombre'
        ]
        if(all(name in content.keys() for name in necessary_data)):
            result = db.post_estudiante(content)
            return {'status': 0, 'data':result}, 200
        else:
            return {'status': 1, 'msg':'Data no valida'}, 400
    def put(self, id):
        content = request.json
        necessary_data = [
            'carnet',
            'nombre'
        ]
        if(all(name in content.keys() for name in necessary_data)):
            data = db.update_estudiante(id, content)
            result = {'status': 0, 'data':data}
            print(result)
            return result, 200
        else:
            return {'status': 1, 'msg':'Data no valida'}, 400

    def delete(self, id):
        data = db.delete_estudiante(id)
        result = {'status': 0, 'data':data}
        return result


@api.resource('/estudiantes')
class Estudiantes(Resource):
    def get(self):
        data = db.get_all_estudiantes()
        result = {'status': 0, 'data':data}
        print(result)
        return result


    
if __name__ == '__main__':
    app.run(host='0.0.0.0', port=3000, threaded=True, use_reloader=True)
