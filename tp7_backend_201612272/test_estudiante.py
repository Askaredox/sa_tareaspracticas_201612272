import unittest
import requests
import simplejson as json

class TestEstudiante(unittest.TestCase):
    URL = 'http://172.19.0.3:3000'
    id_e = 12
    def test_01_check(self):
        r = requests.get(self.URL)
        self.assertEqual(r.status_code, 200)
        self.assertTrue(r.json()['ok'])

    def test_02_post_estudiante(self):
        data = {
            'id':self.id_e,
            'carnet':201612272,
            'nombre':'Andres Carvajal'
        }
        r = requests.post(f'{self.URL}/estudiante', json=data)
        self.assertEqual(r.status_code, 200, r.json())
        self.assertDictEqual(r.json(),{'status': 0, 'data':1})

    def test_03_get_estudiante(self):
        r = requests.get(f'{self.URL}/estudiante/{self.id_e}')
        expected = {
            'id':self.id_e,
            'carnet':201612272,
            'nombre':'Andres Carvajal',
        }
        self.assertEqual(r.status_code, 200)
        data = r.json()['data']
        self.assertEqual(data['id'], expected['id'])
        self.assertEqual(data['carnet'], expected['carnet'])
        self.assertEqual(data['nombre'], expected['nombre'])

    def test_04_get_all_estudiante(self):
        r = requests.get(f'{self.URL}/estudiantes')
        self.assertEqual(r.status_code, 200)
        data = r.json()
        self.assertGreaterEqual(len(data['data']),1)

    def test_05_update_estudiante(self):
        data = {
            'carnet':201612272,
            'nombre':'Natalia Matamoros',
        }
        r = requests.put(f'{self.URL}/estudiante/{self.id_e}', json=data)
        data = r.json()
        self.assertEqual(r.status_code, 200, data)
        self.assertEqual(data,{'status': 0, 'data':1})

    def test_06_get_estudiante(self):
        r = requests.get(f'{self.URL}/estudiante/{self.id_e}')
        expected = {
            'id':self.id_e,
            'carnet':201612272,
            'nombre':'Natalia Matamoros',
        }
        self.assertEqual(r.status_code, 200)
        data = r.json()['data']
        self.assertEqual(data['id'], expected['id'])
        self.assertEqual(data['carnet'], expected['carnet'])
        self.assertEqual(data['nombre'], expected['nombre'])

    def test_08_delete_estudiante(self):
        r = requests.delete(f'{self.URL}/estudiante/{self.id_e}')
        self.assertEqual(r.status_code, 200)

