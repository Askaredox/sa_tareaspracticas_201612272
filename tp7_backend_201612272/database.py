import pymysql
import os

class Sql:
    def __init__(self): # TODO: colocar información de base de datos
        self.connection = pymysql.connect(
            host=os.environ['DB_HOST'],
            user=os.environ['DB_USER'],
            passwd=os.environ['DB_PASS'], 
            db=os.environ['DB_DATABASE'], 
            cursorclass=pymysql.cursors.DictCursor, 
            sql_mode=''
        )

    def get_estudiante(self, id):
        query = '''
            SELECT * FROM Estudiante e
            WHERE e.id = %s
        '''
        with self.connection.cursor() as cursor:
            cursor.execute(query, (id,))
            ret = cursor.fetchone()
            data = ret
        return data

    def post_estudiante(self, estudiante):
        if('id' in estudiante.keys()):
            query = '''
                INSERT INTO Estudiante(id, carnet, nombre)
                VALUES (%s,%s,%s)
            '''
            with self.connection.cursor() as cursor:
                data = cursor.execute(query,
                    (estudiante['id'], estudiante['carnet'],estudiante['nombre'])
                )
        else:
            query = '''
                INSERT INTO Estudiante(carnet, nombre)
                VALUES (%s,%s)
            '''
            with self.connection.cursor() as cursor:
                data = cursor.execute(query,
                    (estudiante['carnet'],estudiante['nombre']),
                )
        
        self.connection.commit()
        return data

    def update_estudiante(self, id, estudiante):
        query = '''
            UPDATE Estudiante e
            SET 
                carnet = %s,
                nombre = %s
            WHERE e.id = %s
        '''
        with self.connection.cursor() as cursor:
            data = cursor.execute(query, (
                estudiante['carnet'],
                estudiante['nombre'],
                id
            ))
        self.connection.commit()
        return data


    def delete_estudiante(self, id):
        query = '''
            DELETE FROM Estudiante e WHERE e.id=%s
        '''
        with self.connection.cursor() as cursor:
            data = cursor.execute(query, (id,))
        self.connection.commit()
        return data


    def get_all_estudiantes(self):
        query = '''
            SELECT * FROM Estudiante e
        '''
        with self.connection.cursor() as cursor:
            cursor.execute(query)
            ret = cursor.fetchall()
            data = ret
        return data




