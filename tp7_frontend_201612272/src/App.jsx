import React from "react";
import {
  Table,
  Message,
  toaster,
  Modal,
  Button,
  Input,
  IconButton,
} from "rsuite";
import { Plus, Edit, Trash } from "@rsuite/icons";
import Service from "./service";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      popupOpen: false,
      id: -1,
      nombre: "",
      carnet: "",
    };
    this.popupStatus = "add";
  }

  async componentDidMount() {
    this.updateList();
  }

  render() {
    return (
      <div style={{ width: "80%", margin: "0 auto" }}>
        <h1>Estudiantes</h1>
        <IconButton
          style={{ float: "right" }}
          appearance="primary"
          color="green"
          icon={<Plus />}
          onClick={() => this.openPopup("add", "", "")}
        >
          Add
        </IconButton>
        <br />
        <br />
        <Table
          height={400}
          data={this.state.data}
        >
          <Table.Column width={70} align="center" fixed>
            <Table.HeaderCell>Id</Table.HeaderCell>
            <Table.Cell dataKey="id" />
          </Table.Column>

          <Table.Column flexGrow={1} fixed>
            <Table.HeaderCell>Nombre</Table.HeaderCell>
            <Table.Cell dataKey="nombre" />
          </Table.Column>

          <Table.Column flexGrow={1}>
            <Table.HeaderCell>Carnet</Table.HeaderCell>
            <Table.Cell dataKey="carnet" />
          </Table.Column>

          <Table.Column width={120} fixed="right">
            <Table.HeaderCell>Action</Table.HeaderCell>

            <Table.Cell>
              {(rowData) => {
                return (
                  <span>
                    <IconButton icon={<Edit />} appearance="primary" size="xs" color='blue' onClick={() => this.editEstudiante(rowData)}/>
                    <IconButton icon={<Trash />} appearance="primary" size="xs" color='red' onClick={() => this.deleteEstudiante(rowData)}/>
                  </span>
                );
              }}
            </Table.Cell>
          </Table.Column>
        </Table>
        <Modal
          backdrop={true}
          keyboard={false}
          open={this.state.popupOpen}
          onClose={this.closePopup}
        >
          <Modal.Header>
            <Modal.Title>Ingresar datos</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <label>Estudiante</label>
            <Input
              placeholder="Ingrese el nombre"
              value={this.state.nombre}
              onChange={(val) => this.setState({ nombre: val })}
            />
            <hr />
            <label>Carnet</label>
            <Input
              placeholder="Ingrese el carnet"
              value={this.state.carnet}
              onChange={(va) => this.setState({ carnet: va })}
            />
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.popupConfirm} appearance="primary">
              Ok
            </Button>
            <Button onClick={this.closePopup} appearance="subtle">
              Cancel
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
  popupConfirm = async () => {
    let estudiante = {
      nombre: this.state.nombre,
      carnet: this.state.carnet,
    };
    let placement = "topCenter";
    let result =
      this.popupStatus === "add"
        ? await Service.postEstudiante(estudiante)
        : await Service.updateEstudiante(this.state.id, estudiante);
    
    
    if (result.status === 0) {
      let message = (
        <Message showIcon type={"success"}>
          {this.popupStatus === "add"
          ? `El estudiante ${estudiante.nombre} ha sido añadido exitósamente.`
          : `El estudiante ${estudiante.nombre} ha sido editado exitósamente.`}
        </Message>
      );
      toaster.push(message, { placement });
    }
    await this.updateList();
    this.closePopup();
  };
  closePopup = () => {
    this.setState({
      popupOpen: false,
    });
  };

  openPopup = (status, id, nombre, carnet) => {
    this.setState({
      popupOpen: true,
      id: id,
      nombre: nombre,
      carnet: carnet,
    });
    this.popupStatus = status;
  };
  editEstudiante = (rowData) => {
    this.openPopup("edit", rowData.id, rowData.nombre, rowData.carnet);
  };
  deleteEstudiante = async (rowData) => {
    let result = await Service.deleteEstudiante(rowData.id);
    let placement = "topCenter";
    if (result.data === 1) {
      let message = (
        <Message showIcon type={"success"}>
          El estudiante {rowData.nombre} ha sido eliminado exitósamente.
        </Message>
      );
      toaster.push(message, { placement });
    }
    this.updateList();
  };
  updateList = async () => {
    let data = await Service.getAllEstudiante();
    this.setState({ data: data.data });
  }
}

export default App;
