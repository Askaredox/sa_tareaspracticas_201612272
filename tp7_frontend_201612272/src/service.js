import axios from "axios";

export default class Service {
    static server = 'http://3.235.237.127:3000';

    static async postEstudiante(data) {
        console.log(data);
        return axios
        .post(Service.server + "/estudiante", data)
        .then((res) => res.data);
    }

    static async getEstudiante(id) {
        return axios
        .get(Service.server + `/estudiante/${id}`)
        .then((res) => res.data);
    }

    static async getAllEstudiante() {
        return axios
        .get(Service.server + `/estudiantes`)
        .then((res) => res.data);
    }

    static async updateEstudiante(id, data) {
        return axios
            .put(Service.server + `/estudiante/${id}`, data)
            .then((res) => res.data);
    }

    static async deleteEstudiante(id) {
        return axios
            .delete(Service.server + `/estudiante/${id}`)
            .then((res) => res.data);
    }
}
